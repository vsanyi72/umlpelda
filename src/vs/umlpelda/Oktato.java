package vs.umlpelda;

import java.util.Date;

public class Oktato extends Szemely 
{

	private String[] programNyelvek;
	
	public Oktato(String vezetekNev, String keresztNev, Date szuletesDatum) {
		super(vezetekNev, keresztNev, szuletesDatum);
	
	}

	public String[] getProgramNyelvek() {
		return programNyelvek;
	}

	public void setProgramNyelvek(String[] programNyelvek) {
		this.programNyelvek = programNyelvek;
	}
	
}
