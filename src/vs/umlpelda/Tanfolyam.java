package vs.umlpelda;

import java.util.Date;

public class Tanfolyam 
{
	private Oktato oktato;
	private Diak[] diakok;
	private Date indulasIdo;
	
	public Tanfolyam(Oktato oktato, Diak[] diakok, Date indulasIdo) {
		super();
		this.oktato = oktato;
		this.diakok = diakok;
		this.indulasIdo = indulasIdo;
	}

	public Oktato getOktato() {
		return oktato;
	}

	public void setOktato(Oktato oktato) {
		this.oktato = oktato;
	}

	public Diak[] getDiakok() {
		return diakok;
	}

	public void setDiakok(Diak[] diakok) {
		this.diakok = diakok;
	}

	public Date getIndulasIdo() {
		return indulasIdo;
	}

	public void setIndulasIdo(Date indulasIdo) {
		this.indulasIdo = indulasIdo;
	}
	
}
