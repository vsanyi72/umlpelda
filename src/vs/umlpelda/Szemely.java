package vs.umlpelda;

import java.util.Date;

abstract class Szemely 
{
	private String vezetekNev;
	private String keresztNev;
	private String kozepsoNev;
	private Date szuletesDatum;
	public Szemely(String vezetekNev, String keresztNev, String kozepsoNev, Date szuletesDatum) {
		super();
		this.vezetekNev = vezetekNev;
		this.keresztNev = keresztNev;
		this.kozepsoNev = kozepsoNev;
		this.szuletesDatum = szuletesDatum;
	}
	public Szemely(String vezetekNev, String keresztNev, Date szuletesDatum) {
		super();
		this.vezetekNev = vezetekNev;
		this.keresztNev = keresztNev;
		this.szuletesDatum = szuletesDatum;
	}
	public String getVezetekNev() {
		return vezetekNev;
	}
	public void setVezetekNev(String vezetekNev) {
		this.vezetekNev = vezetekNev;
	}
	public String getKeresztNev() {
		return keresztNev;
	}
	public void setKeresztNev(String keresztNev) {
		this.keresztNev = keresztNev;
	}
	public String getKozepsoNev() {
		return kozepsoNev;
	}
	public void setKozepsoNev(String kozepsoNev) {
		this.kozepsoNev = kozepsoNev;
	}
	public Date getSzuletesDatum() {
		return szuletesDatum;
	}
	public void setSzuletesDatum(Date szuletesDatum) {
		this.szuletesDatum = szuletesDatum;
	}
}
